## Python

We will use [Virtualenv](https://virtualenv.pypa.io/en/latest/) to maintain portable projects in Python.  
Packaging your work in a virtual environment allows someone else to replicate the environment (i.e., installed packages, environment variables) in which the project was created, and thus be able to run it on their local machine.

#### First Steps:
1. Install Virtualenv.  
`$ pip install virtualenv`.  

2. Create a virtual environment named "thunderdome" for this project, then activate it.  
`$ virtualenv thunderdome`  
then on a Mac: `$ source thunderdome/Scripts/activate`  
or for Windows: `$ thunderdome\Scripts\activate`

3. Install the packages from requirements.txt into your virtual environment.  Navigate to the python folder of your thunderdome repo, then run the following:
`$ pip install -r requirements.txt`

4. Install a new kernel for your Jupyter Notebook.  
`$ ipython kernel install --user --name=thunderdome`

5. Copy the contents of `.env_example` into a new file named `.env`, then edit `.env` to contain the appropriate values. For this project, you will need to copy over the SQL connection details.

6. Open a Jupyter Notebook.  
`$ jupyter notebook`  
This will launch a file system in your browser. From here, you can create and edit scripts and use notebooks to execute Python commands.

7. Open the example notebook (`Exploration.ipynb`).  
You will probably need to change the kernel to the one you created in step 4:  _Kernel > Change kernel > {basename}_  
Use the notebook to confirm that your SQL connection works, and use this as a starting place for your analysis. Typically, Jupyter is used for exploration and initial analysis. Create scripts as you go to perform any repeatable procedures.  


#### As You Go:
1. You may want to edit `.env` to include additional project-specific environment variables. If you do so, make sure that you update `.env_example` accordingly, so that other users know which variables need to be assigned.  

2. You may need to install additional packages. Be sure to install while the virtual environment is activated and then update `requirements.txt` with the packages:  
`$ pip install <package>`  
`$ pip freeze -r > requirements.txt`  

3. To exit the virtual environment, simply type `$ deactivate` in the root project directory.


#### Project files:  
| | |
| --- | --- |
| `requirements.txt` | Specifies any necessary packages. These will be automatically installed within the virtual environment upon activation. |
| `.env_example` | A template of the environment variables that need to be set. _Do not include the actual values in this file_. The user will make a copy of this file (`.env`) in which the true values are stored. |
| `../.gitignore` | Files that you don't want to track in version control. Make sure you are not tracking your environment variables (`.env`) or the virtual environment directory (`thunderdome`).  |
| `scripts/sql_connect.py` | Connect to the Postgres database and load tables as Pandas dataframes |
| `Exploration.ipynb` | Use this notebook to test your Postgres connection and begin your analysis |
