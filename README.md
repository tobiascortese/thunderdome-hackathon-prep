# Welcome to the Thunderdome

Few rivalries are as storied as the classic debate between R v. Python. Pythonistas tout the general purpose language as a selling point, while R people swear working in the Tidyverse is out of this world. This team exercise is an opportunity to explore BOTH programming languages, determining once and for all how they both stack up.

## What to expect
Participants will be broken into groups of 3 or 4, with the intention of evenly distributing R, Python and Data Science skills across each group. You will be given access to a database, an Alpha Team mentor, some starter code, but from there it's all team-driven. The goal is to complete your Seattle Housing analysis using both R and Python to get a sense of how the tools compare. You're encouraged to meet with your team as much or as little as you like. Coming out of this activity you should be able to:  
###
- Set up a project in both R and Python  
- Work with a team to define the approach to a real-world question  
- Code the same analysis in R and Python  
- Leverage BitBucket for source control  

## Thunderdome Schedule
### Kick-off on June 20:
- Review the structure of the hackathon, including rules, objectives, and data
- Assign teams (predefined based on survey responses and an element of randomness)
- Spend time getting everyone set up with the various tools

### Midpoint Check-in on July 11:
- Review teams progress to date
- troubleshoot any remaining issues as a group

### Final Presentation on August 8:
- Present out findings and key R v Python takeaways
- Crown an ultimate programming language champion (just kidding)

## Recommended steps to take prior to attending the kick-off:
-	Install R and RStudio Desktop https://www.rstudio.com/products/rstudio/download/
-	Install Python via Anaconda: https://www.anaconda.com/distribution/ and follow the instructions here: https://medium.com/@GalarnykMichael/install-python-on-windows-anaconda-c63c7c3d1444
-	Install Git: https://git-scm.com/
-	Create an account on Bitbucket with your Slalom email: https://bitbucket.org/product/
-	Install pgAdmin: https://www.pgadmin.org/ 
-	Send an email to support@slalom.com asking to be added to the bitbucket “slalom-consulting:seattle-ima-developers” user group
-	Practice writing some code in both R and Python
-	Get pumped up!


